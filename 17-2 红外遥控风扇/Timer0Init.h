#ifndef __TIMER0INIT_H__
#define	__TIMER0INIT_H__
void Timer0Init();
void SetCounter(unsigned int counter);
unsigned int GetCounter(void);
void Timer0Run(unsigned symble);
#endif