#include <REGX52.H>

void Timer0Init()		//1毫秒@12.000MHz
{
	TMOD &= 0xF0;		//设置定时器模式
	TMOD |= 0x01;		//设置定时器模式
	TL0 = 0;		//设置定时初值
	TH0 = 0;		//设置定时初值
	TF0 = 0;		//清除TF0标志
	TR0 = 0;		//定时器0开始计时

}
void SetCounter(unsigned int counter)
{
	TH0=counter/256;
	TL0=counter%256;	
}
unsigned int GetCounter(void)
{
	return (TH0<<8)|TL0;
}
void Timer0Run(unsigned char symble)
{
	TR0=symble;//决定计数器的启动
}