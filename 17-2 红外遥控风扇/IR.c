#include <REGX52.H>
#include "Timer0Init.h"
#include "outInit_0.h"

unsigned int IR_Time;//计时
unsigned char IR_State;//状态

unsigned char IR_Data[4];//Address+反Address+Command+反Command(32位)Data存的只是数据缓存
unsigned char IR_pData;//看Data存到第几位，要存32位

unsigned char IR_DataFlag;//pData存满32位就发出1的标志
unsigned char IR_RepeatFlag;//重发标志
unsigned char IR_Address;//存Address地址的最终结果（把Data[0]里的转移过来）
unsigned char IR_Command;

void IR_Init(void)
{
	Timer0Init();//只拿来计数，不中断服务。红外占用定时器0，待会电机就放定时器1
	outIinit_0();//用来下面中断服务
}

unsigned char IR_GetDataFlag(void)//判断是否收到Data,收到为1，未成功为0
{
	if(IR_DataFlag)
	{
		IR_DataFlag=0;
		return 1;
	}
	return 0;
}

unsigned char IR_GetRepeatFlag(void)//同上，不过是在repeat时
{
	if(IR_RepeatFlag)
	{
		IR_RepeatFlag=0;
		return 1;
	}
	return 0;
}

unsigned char IR_GetAddress(void)
{
	return IR_Address;
}

unsigned char IR_GetCommand(void)
{
	return IR_Command;
}

void Int0_Routine(void) interrupt 0
{
	if(IR_State==0)//在空闲状态时（我弄了三个状态来判断）			
	{
		SetCounter(0);//计数器清零要开始计数喽~
		Timer0Run(1);//计数器启动！		
		IR_State=1;//进入1状态			
	}
	else if(IR_State==1)//在1状态时	
	{
		IR_Time=GetCounter();//获取时间
		SetCounter(0);//已获得一个时间，要清零为了下一次获另一个时间	
		if(IR_Time>12442-500 && IR_Time<12442+500)//证明是start信号，有误差，上下波动可接受
		{
			IR_State=2;//进入2状态，开始解码		
		}					
		else if(IR_Time>10368-500 && IR_Time<10368+500)//证明是repeat信号，上下500不会和12442+-500重叠
		{
			IR_RepeatFlag=1;//标志接受完Data了	
			Timer0Run(0);//把计数器停了		
			IR_State=0;//任务完成，使回到空闲0状态			
		}
		else//其他情况，比如有可能信号出错，就重来					
		{
			IR_State=1;//回到搜寻启动信号的1状态			
		}
	}
	else if(IR_State==2)//进入解码2状态		
	{
		IR_Time=GetCounter();//上一个终端的时间要保留下来	
		SetCounter(0);//计数器清零
		if(IR_Time>1032-500 && IR_Time<1032+500)//判断是否为“0”
		{
			IR_Data[IR_pData/8]&=~(0x01<<(IR_pData%8));//0~31折成四个字节，依次存0，全部置零
			IR_pData++;//看存到第几位		
		}
		else if(IR_Time>2074-500 && IR_Time<2074+500)//判断是否为“1”
		{
			IR_Data[IR_pData/8]|=(0x01<<(IR_pData%8));//全部置1	
			IR_pData++;		
		}
		else//其他情况，出错什么的				
		{
			IR_pData=0;//清零		
			IR_State=1;//回1状态		
		}
		if(IR_pData>=32)//判断是否收完数据了	
		{
			IR_pData=0;//清0，为了下一次收数据		
			if((IR_Data[0]==~IR_Data[1]) && (IR_Data[2]==~IR_Data[3]))//判断Datad对不对，因为Address后是它反码，Command同理	
			{
				IR_Address=IR_Data[0];//数据及时转移，以防止丢失还是什么的
				IR_Command=IR_Data[2];
				IR_DataFlag=1;//代表已经接收到数据	
			}
			Timer0Run(0);//计数器停止		
			IR_State=0;	//2状态任务完成，回到空闲0状态		
		}
	}
}
