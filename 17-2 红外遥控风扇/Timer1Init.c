#include <REGX52.H>

void Timer1Init()		//1毫秒@12.000MHz
{
	TMOD &= 0x0F;		//设置定时器模式
	TMOD |= 0x10;		//设置定时器模式
	TL1 = 0x9C;		//设置定时初值
	TH1 = 0xFF;		//设置定时初值
	TF1 = 0;		//清除TF1标志
	TR1 = 1;		//定时器1开始计时
	ET1=1;
	EA=1;
	PT1=0;
}
//void Timer1routine() interrupt 3
//{
//	int c=0;
//	c++;
//	TL1 = 0x18;	
//	TH1 = 0xFC;
//	if(c>=1000)
//	{
//		c=0;
//
//	}
//}